/* Copyright (C) 2018, 2019, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2016-2018 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "solparser.h"
#include "test_solstice_utils.h"

static const char* input[] = {
  "- material: &lambertian\n",
  "    matte: { reflectivity: 0.5 }\n",
  "- geometry: &cylinder\n",
  "    - cylinder: { radius: 1, height: 10, slices: 128 }\n",
  "      material: *lambertian\n",
  "- geometry: &cylinder2\n",
  "    - cylinder: { radius: 1, height: 10 }\n",
  "      material: *lambertian\n",
  "- geometry: &hyperbol1\n",
  "    - hyperbol:\n",
  "        focals: &hyp1_focals { real: 4, image: 1 }\n",
  "        clip:\n",
  "        - operation : AND\n",
  "          vertices : [[1, 2],[3, 4],[6, 7]]\n",
  "      material: *lambertian\n",
  "- geometry: &hemisphere1\n",
  "    - hemisphere:\n",
  "        radius: 100\n",
  "        clip:\n",
  "        - operation : AND\n",
  "          circle: { radius: 4 }\n",
  "      material: *lambertian\n",
  "- sun: \n",
  "    dni: 1\n",
  "    spectrum: [{wavelength: 1, data: 1}]\n",
  "- atmosphere:\n",
  "    extinction: [{wavelength: 1, data: 1}]\n",
  "- entity:\n",
  "    name: entity0\n",
  "    primary: 0\n",
  "    geometry: *cylinder\n",
  "    anchors:\n",
  "      - name: anchor0\n",
  "        position: [1, 2, 3]\n",
  "      - name: anchor1\n",
  "        position: [4, 5, 6]\n",
  "      - name: anchor2\n",
  "        hyperboloid_image_focals: *hyp1_focals\n",
  "    children:\n",
  "      - name: entity0a\n",
  "        primary: 1\n",
  "        geometry: *cylinder2\n",
  "      - name: entity0b\n",
  "        primary: 1\n",
  "        geometry: *cylinder\n",
  "        anchors:\n\n",
  "          - name: anchor0\n",
  "            position: [4, 5, 6]\n",
  "          - name: entity0b\n",
  "            position: [7, 8, 9]\n",
  "      - name: entity0c\n",
  "        primary: 0\n",
  "        geometry: *hyperbol1\n",
  "      - name: entity0d\n",
  "        primary: 0\n",
  "        geometry: *hemisphere1\n",
  "- entity:\n",
  "    name: entity1\n",
  "    x_pivot:\n",
  "      ref_point: [1, 2, 3]\n",
  "      target: { anchor: \"entity0.entity0b.anchor0\" }\n",
  "- entity:\n",
  "    name: entity2\n",
  "    zx_pivot:\n",
  "      spacing: 1\n",
  "      ref_point: [1, 2, 3]\n",
  "      target: { anchor: \"entity0.entity0b.anchor0\" }\n",
  "- entity:\n",
  "    name: entity3\n",
  "    x_pivot:\n",
  "      ref_point: [4, 2, 3]\n",
  "      target: { anchor: \"entity0.anchor2\" }\n",
  NULL
};

const struct solparser_anchor* entity0_anchor0;
const struct solparser_anchor* entity0_anchor1;
const struct solparser_anchor* entity0_entity0b_anchor0;
const struct solparser_anchor* entity0_entity0b_entity0b;
const struct solparser_geometry* geom;
const struct solparser_x_pivot* x_pivot;
const struct solparser_zx_pivot* zx_pivot;

static void
check_entity0
  (struct solparser* parser, const struct solparser_entity* entity0)
{
  struct solparser_anchor_id anchor_id;
  struct solparser_entity_id entity_id;
  struct solparser_object_id obj_id;
  const struct solparser_entity *entity0a, *entity0b, *entity0c, *entity0d;
  const struct solparser_material_matte* matte;
  const struct solparser_material* mtl;
  const struct solparser_material_double_sided* mtl2;
  const struct solparser_object* obj;
  const struct solparser_shape* shape;
  const struct solparser_shape_cylinder* cylinder;
  double tmp[3];

  CHK(parser != NULL);
  CHK(entity0 != NULL);

  CHK(strcmp(str_cget(&entity0->name), "entity0") == 0);
  CHK(d3_eq(entity0->rotation, d3_splat(tmp, 0)) == 1);
  CHK(d3_eq(entity0->translation, d3_splat(tmp, 0)) == 1);
  CHK(entity0->type == SOLPARSER_ENTITY_GEOMETRY);

  geom = solparser_get_geometry(parser, entity0->data.geometry);
  CHK(solparser_geometry_get_objects_count(geom) == 1);

  obj_id = solparser_geometry_get_object(geom, 0);
  obj = solparser_get_object(parser, obj_id);
  CHK(d3_eq(obj->rotation, d3_splat(tmp, 0)) == 1);
  CHK(d3_eq(obj->translation, d3_splat(tmp, 0)) == 1);

  shape = solparser_get_shape(parser, obj->shape);
  CHK(shape->type == SOLPARSER_SHAPE_CYLINDER);

  cylinder = solparser_get_shape_cylinder(parser, shape->data.cylinder);
  CHK(cylinder->height == 10);
  CHK(cylinder->radius == 1);
  CHK(cylinder->nslices == 128);

  mtl2 = solparser_get_material_double_sided(parser, obj->mtl2);
  CHK(mtl2->front.i == mtl2->back.i);

  mtl = solparser_get_material(parser, mtl2->front);
  CHK(mtl->type == SOLPARSER_MATERIAL_MATTE);

  matte = solparser_get_material_matte(parser, mtl->data.matte);
  CHK(matte->reflectivity.type == SOLPARSER_MTL_DATA_REAL);
  CHK(matte->reflectivity.value.real == 0.5);

  CHK(solparser_entity_get_children_count(entity0) == 4);
  CHK(solparser_entity_get_anchors_count(entity0) == 3);

  anchor_id = solparser_entity_get_anchor(entity0, 0);
  entity0_anchor0 = solparser_get_anchor(parser, anchor_id);
  CHK(strcmp(str_cget(&entity0_anchor0->name), "anchor0") == 0);
  CHK(d3_eq(entity0_anchor0->position, d3(tmp, 1, 2, 3)) == 1);

  anchor_id = solparser_entity_get_anchor(entity0, 1);
  entity0_anchor1 = solparser_get_anchor(parser, anchor_id);
  CHK(strcmp(str_cget(&entity0_anchor1->name), "anchor1") == 0);
  CHK(d3_eq(entity0_anchor1->position, d3(tmp, 4, 5, 6)) == 1);

  entity_id = solparser_entity_get_child(entity0, 0);
  entity0a = solparser_get_entity(parser, entity_id);
  CHK(strcmp(str_cget(&entity0a->name), "entity0a") == 0);
  CHK(entity0a->type == SOLPARSER_ENTITY_GEOMETRY);
  CHK(entity0->data.geometry.i != entity0a->data.geometry.i);
  CHK(solparser_entity_get_anchors_count(entity0a) == 0);
  CHK(solparser_entity_get_children_count(entity0a) == 0);

  geom = solparser_get_geometry(parser, entity0a->data.geometry);
  CHK(solparser_geometry_get_objects_count(geom) == 1);
  obj_id = solparser_geometry_get_object(geom, 0);
  obj = solparser_get_object(parser, obj_id);
  shape = solparser_get_shape(parser, obj->shape);
  CHK(shape->type == SOLPARSER_SHAPE_CYLINDER);
  cylinder = solparser_get_shape_cylinder(parser, shape->data.cylinder);
  CHK(cylinder->height == 10);
  CHK(cylinder->radius == 1);
  CHK(cylinder->nslices == 16);

  entity_id = solparser_entity_get_child(entity0, 1);
  entity0b = solparser_get_entity(parser, entity_id);
  CHK(strcmp(str_cget(&entity0b->name), "entity0b") == 0);
  CHK(entity0b->type == SOLPARSER_ENTITY_GEOMETRY);
  CHK(entity0->data.geometry.i == entity0b->data.geometry.i);
  CHK(solparser_entity_get_anchors_count(entity0b) == 2);
  CHK(solparser_entity_get_children_count(entity0b) == 0);

  anchor_id = solparser_entity_get_anchor(entity0b, 0);
  entity0_entity0b_anchor0 = solparser_get_anchor(parser, anchor_id);
  CHK(strcmp(str_cget(&entity0_entity0b_anchor0->name), "anchor0") == 0);
  CHK(d3_eq(entity0_entity0b_anchor0->position, d3(tmp, 4, 5, 6)) == 1);

  anchor_id = solparser_entity_get_anchor(entity0b, 1);
  entity0_entity0b_entity0b = solparser_get_anchor(parser, anchor_id);
  CHK(strcmp(str_cget(&entity0_entity0b_entity0b->name), "entity0b") == 0);
  CHK(d3_eq(entity0_entity0b_entity0b->position, d3(tmp, 7, 8, 9)) == 1);

  entity_id = solparser_entity_get_child(entity0, 2);
  entity0c = solparser_get_entity(parser, entity_id);
  CHK(strcmp(str_cget(&entity0c->name), "entity0c") == 0);
  CHK(entity0c->type == SOLPARSER_ENTITY_GEOMETRY);
  CHK(entity0->data.geometry.i != entity0c->data.geometry.i);
  CHK(solparser_entity_get_anchors_count(entity0c) == 0);
  CHK(solparser_entity_get_children_count(entity0c) == 0);

  entity_id = solparser_entity_get_child(entity0, 3);
  entity0d = solparser_get_entity(parser, entity_id);
  CHK(strcmp(str_cget(&entity0d->name), "entity0d") == 0);
  CHK(entity0d->type == SOLPARSER_ENTITY_GEOMETRY);
  CHK(entity0->data.geometry.i != entity0d->data.geometry.i);
  CHK(solparser_entity_get_anchors_count(entity0d) == 0);
  CHK(solparser_entity_get_children_count(entity0d) == 0);
}

static void
check_entity1
  (struct solparser* parser, const struct solparser_entity* entity1)
{
  double tmp[3];

  CHK(parser != NULL);
  CHK(entity1 != NULL);

  CHK(strcmp(str_cget(&entity1->name), "entity1") == 0);
  CHK(entity1->type == SOLPARSER_ENTITY_X_PIVOT);
  CHK(solparser_entity_get_anchors_count(entity1) == 0);
  CHK(solparser_entity_get_children_count(entity1) == 0);

  x_pivot = solparser_get_x_pivot(parser, entity1->data.x_pivot);
  CHK(x_pivot != NULL);
  CHK(d3_eq(x_pivot->ref_point, d3(tmp, 1, 2, 3)) == 1);
  CHK(x_pivot->target.type == SOLPARSER_TARGET_ANCHOR);
}

static void
check_entity2
  (struct solparser* parser, const struct solparser_entity* entity2)
{
  double tmp[3];

  CHK(parser != NULL);
  CHK(entity2 != NULL);

  CHK(strcmp(str_cget(&entity2->name), "entity2") == 0);
  CHK(entity2->type == SOLPARSER_ENTITY_ZX_PIVOT);
  CHK(solparser_entity_get_anchors_count(entity2) == 0);
  CHK(solparser_entity_get_children_count(entity2) == 0);

  zx_pivot = solparser_get_zx_pivot(parser, entity2->data.zx_pivot);
  CHK(zx_pivot != NULL);
  CHK(zx_pivot->spacing == 1);
  CHK(d3_eq(zx_pivot->ref_point, d3(tmp, 1, 2, 3)) == 1);
  CHK(zx_pivot->target.type == SOLPARSER_TARGET_ANCHOR);
}

static void
check_entity3
  (struct solparser* parser, const struct solparser_entity* entity3)
{
  double tmp[3];

  CHK(parser != NULL);
  CHK(entity3 != NULL);

  CHK(strcmp(str_cget(&entity3->name), "entity3") == 0);
  CHK(entity3->type == SOLPARSER_ENTITY_X_PIVOT);
  CHK(solparser_entity_get_anchors_count(entity3) == 0);
  CHK(solparser_entity_get_children_count(entity3) == 0);

  x_pivot = solparser_get_x_pivot(parser, entity3->data.x_pivot);
  CHK(x_pivot != NULL);
  CHK(d3_eq(x_pivot->ref_point, d3(tmp, 4, 2, 3)) == 1);
  CHK(x_pivot->target.type == SOLPARSER_TARGET_ANCHOR);
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct solparser* parser;
  struct solparser_entity_iterator it, it_end;
  const struct solparser_anchor* anchor;
  FILE* stream;
  size_t i;
  (void)argc, (void)argv;

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);
  solparser_create(&allocator, &parser);

  stream = tmpfile();
  CHK(stream != NULL);
  i = 0;
  while(input[i]) {
    const size_t len = strlen(input[i]);
    CHK(fwrite(input[i], 1, len, stream) == len);
    ++i;
  }
  rewind(stream);

  CHK(solparser_setup(parser, NULL, stream) == RES_OK);
  CHK(solparser_load(parser) == RES_OK);

  solparser_entity_iterator_begin(parser, &it);
  solparser_entity_iterator_end(parser, &it_end);
  CHK(solparser_entity_iterator_eq(&it, &it_end) == 0);

  while(!solparser_entity_iterator_eq(&it, &it_end)) {
    struct solparser_entity_id entity_id;
    const struct solparser_entity* entity;

    entity_id = solparser_entity_iterator_get(&it);
    entity = solparser_get_entity(parser, entity_id);

    if(!strcmp(str_cget(&entity->name), "entity0")) {
      check_entity0(parser, entity);
    }
    else if (!strcmp(str_cget(&entity->name), "entity1")) {
      check_entity1(parser, entity);
    }
    else if(!strcmp(str_cget(&entity->name), "entity2")) {
      check_entity2(parser, entity);
    }
    else if (!strcmp(str_cget(&entity->name), "entity3")) {
      check_entity3(parser, entity);
    } else {
      FATAL("Unexpected entity name.\n");
    }

    solparser_entity_iterator_next(&it);
  }

  anchor = solparser_get_anchor(parser, x_pivot->target.data.anchor);
  CHK(anchor == entity0_entity0b_anchor0);

  anchor = solparser_find_anchor(parser, "entity0");
  CHK(anchor == NULL);
  anchor = solparser_find_anchor(parser, "entity0.anchor0");
  CHK(anchor == entity0_anchor0);
  anchor = solparser_find_anchor(parser, "entity0.anchor1");
  CHK(anchor == entity0_anchor1);
  anchor = solparser_find_anchor(parser, "entity0.entity0a.anchor0");
  CHK(anchor == NULL);
  anchor = solparser_find_anchor(parser, "entity0.entity0b.anchor0");
  CHK(anchor == entity0_entity0b_anchor0);
  anchor = solparser_find_anchor(parser, "entity0.entity0b.entity0b");
  CHK(anchor == entity0_entity0b_entity0b);
  anchor = solparser_find_anchor(parser, "entity1.entity0b.anchor1");
  CHK(anchor == NULL);

  CHK(solparser_load(parser) == RES_BAD_OP);
  solparser_ref_put(parser);
  fclose(stream);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}
